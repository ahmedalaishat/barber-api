/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/27/2020
 * Time: 4:19 PM
 */
const router = require('express').Router();
const AppointmentController = require('../controller/AppointmentController');
const authController = require('../controller/AuthController');

router.get('/barber', authController.verifyToken('barber-token'), AppointmentController.showForBarber);
router.get('/user', authController.verifyToken('user-token'), AppointmentController.showForUser);
router.get('/:appointmentID', AppointmentController.showById);
router.post('/', authController.verifyToken('user-token'), AppointmentController.store);

module.exports = router;
