const router = require('express').Router();
const barberController = require('../controller/BarberController');
const authController = require('../controller/AuthController');

//profile
router.get('/show-profile', authController.verifyToken('barber-token'), barberController.show);
router.put('/edit-profile', authController.verifyToken('barber-token'), barberController.update);
//authentication
//TODO admin authentication(only admin add barbers)
router.post('/register',authController.registerBarber);
router.post('/login',authController.login('barber-login'));

router.route('/')
    .get(authController.verifyToken('user-token'), barberController.index);

router.route('/:barberID')
    .get(authController.verifyToken('user-token'), barberController.show)
    .delete(authController.verifyToken('admin-token'), barberController.destroy);
module.exports = router;
