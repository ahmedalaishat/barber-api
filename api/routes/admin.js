/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/25/2020
 * Time: 4:12 PM
 */
const router = require('express').Router();
const authController = require('../controller/AuthController');

//TODO register only first time
// router.post('/register',authController.registerUser(true));
router.post('/login',authController.login('user-login'));

module.exports = router;
