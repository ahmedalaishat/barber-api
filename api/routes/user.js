const router = require('express').Router();
const userController = require('../controller/UserController');
const authController = require('../controller/AuthController');

//profile
router.get('/show-profile', authController.verifyToken('user-token'), userController.show);
router.put('/edit-profile', authController.verifyToken('user-token'), userController.update);
//authentication
router.post('/register',authController.registerUser(false));
router.post('/login',authController.login('user-login'));

router.route('/')
    .get(authController.verifyToken('user-token'), userController.index);

router.route('/:userID')
    .get(authController.verifyToken('admin-token'), userController.show)
    .delete(authController.verifyToken('admin-token'), userController.destroy);
module.exports = router;
