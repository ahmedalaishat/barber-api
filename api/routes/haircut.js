/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/26/2020
 * Time: 7:59 PM
 */
const router = require('express').Router();
const haircutController = require('../controller/HaircutController');
const authController = require('../controller/AuthController');

router.get('/barber', authController.verifyToken('barber-token'), haircutController.showForBarber);
router.get('/user', authController.verifyToken('user-token'), haircutController.showForUser);
router.get('/:haircutID',haircutController.showById);
router.post('/', authController.verifyToken('barber-token'), haircutController.store);

module.exports = router;
