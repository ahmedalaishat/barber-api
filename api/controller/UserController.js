/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/25/2020
 * Time: 11:26 AM
 */
const User = require('../model/User');

module.exports = {
    index: (req, res, next) => User.find({})
        .then(dishes => res.status(200).send(dishes))
        .catch(err => next(err)),

    show:(req, res, next) => User.findById(req.params.userID||req.user._id)
        ///////should return user not found if user==null
        .then(user => res.status(200).send({user}))
        .catch(err => next(err)),

    update: (req, res, next) =>
        User.findByIdAndUpdate(req.params.userID, {$set: req.body}, {new: true})
            .then(user => res.status(200).send({
                "status": "success",
                "message": "User with id:" + user._id + " updated successfully",
                "Updated user": user
            }))
            .catch(err => next(err)),

    destroy: (req, res, next) => User.findByIdAndDelete(req.params.userID)
        .then(user => res.status(200).send({
            "status": "success",
            "message": "User with id:" + user._id + " deleted successfully",
            "Deleted user": user
        }))
        .catch(err => next(err)),

    // destroyAll: (req, res, next) => User.deleteMany()
    //     .then(result => res.status(200).send({
    //         "status": "success",
    //         "message": "All users deleted successfully",
    //         "result": result
    //     }))
    //     .catch(err => next(err))
};