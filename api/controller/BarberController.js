/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/25/2020
 * Time: 11:26 AM
 */
const Barber = require('../model/Barber');

module.exports = {
    index: (req, res, next) => Barber.find({})
        .then(dishes => res.status(200).send(dishes))
        .catch(err => next(err)),

    show:(req, res, next) => Barber.findById(req.params.barberID||req.user._id)
        ///////should return barber not found if barber==null
        .then(barber => res.status(200).send({barber}))
        .catch(err => next(err)),

    update: (req, res, next) =>
        Barber.findByIdAndUpdate(req.params.barberID, {$set: req.body}, {new: true})
            .then(barber => res.status(200).send({
                "status": "success",
                "message": "Barber with id:" + barber._id + " updated successfully",
                "Updated barber": barber
            }))
            .catch(err => next(err)),

    destroy: (req, res, next) => Barber.findByIdAndDelete(req.params.barberID)
        .then(user => res.status(200).send({
            "status": "success",
            "message": "Barber with id:" + user._id + " deleted successfully",
            "Deleted user": user
        }))
        .catch(err => next(err)),

    // destroyAll: (req, res, next) => Barber.deleteMany()
    //     .then(result => res.status(200).send({
    //         "status": "success",
    //         "message": "All users deleted successfully",
    //         "result": result
    //     }))
    //     .catch(err => next(err))
};