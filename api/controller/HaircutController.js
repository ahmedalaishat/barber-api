/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/26/2020
 * Time: 7:58 PM
 */
const Haircut = require('../model/Haircut');

const show = filterQuery => (req, res, next) => Haircut.find(filterQuery)
    .populate({path: 'barber', select: 'firstName lastName'})
    .populate({path:'user',select:'firstName lastName'})
    .then(haircut => res.status(200).send({haircut}))
    .catch(err => next(err));

module.exports = {
    showById: (req, res, next) => show({_id: req.params.haircutID})(req, res, next),
    showForUser: (req, res, next) => show({user: req.user._id})(req, res, next),
    showForBarber: (req, res, next) => show({barber: req.user._id})(req, res, next),

    store: (req, res, next) => Haircut.create({
        barber: req.user._id,
        user: req.body.user,
        type: req.body.type,
        description: req.body.description
    }).then(haircut => res.status(201).send({
        "status": "success",
        "message": "Haircut with id: " + haircut._id + " created successfully",
        "Created haircut": haircut
    })).catch(err => next(err)),
};