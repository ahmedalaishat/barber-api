/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/27/2020
 * Time: 4:19 PM
 */
const Appointment = require('../model/Appointment');

const show = filterQuery => (req, res, next) => Appointment.find(filterQuery)
    .populate({path: 'barber', select: 'firstName lastName'})
    .populate({path:'user',select:'firstName lastName'})
    .then(appointment => res.status(200).send({appointment}))
    .catch(err => next(err));

module.exports = {
    showById: (req, res, next) => show({_id: req.params.appointmentID})(req, res, next),
    showForUser: (req, res, next) => show({user: req.user._id})(req, res, next),
    showForBarber: (req, res, next) => show({barber: req.user._id})(req, res, next),

    store: (req, res, next) => Appointment.create({
        barber: req.body.barber,
        user: req.user._id,
        date: req.body.date
    }).then(appointment => res.status(201).send({
        "status": "success",
        "message": "Appointment with id: " + appointment._id + " created successfully",
        "Created appointment": appointment
    })).catch(err => next(err)),
};