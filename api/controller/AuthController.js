/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/26/2020
 * Time: 10:17 AM
 */
const User = require('../model/User');
const Barber = require('../model/Barber');
const createError = require('http-errors');
const jsonwebtoken = require('jsonwebtoken');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt; // to extract token from request
const dotenv = require('dotenv');
dotenv.config();

const jwt = {
    generateToken: userOrBarber => jsonwebtoken.sign(
        userOrBarber, process.env.SECRET_KEY, {expiresIn: '10000 days'}),
    opts: {
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: process.env.SECRET_KEY
    }
};
const registerCb = (res, next) => (error, user) =>
    error ? next(createError(error.status || 500, {
            message: error.message || "Registration failed!. Invalid username or password.",
            error
        }))
        : res.send(201, {
            message: 'Registration Successful!.',
            barber: user.toJSON(),
            token: jwt.generateToken({_id: user._id}) || "",
        });

module.exports = {
    init: () => {
        // local strategy(only for login)
        passport.use("user-login", User.createStrategy());
        passport.use("barber-login", Barber.createStrategy());
        passport.serializeUser(User.serializeUser());
        passport.deserializeUser(User.deserializeUser());
        passport.serializeUser(Barber.serializeUser());
        passport.deserializeUser(Barber.deserializeUser());
        ////jwt strategy
        passport.use('user-token', new JwtStrategy(jwt.opts, (jwtPayload, cb) =>
            User.findById(jwtPayload._id, (error, user) => {
                if (error) cb(error, false);
                else if (!user) cb(createError(403, {message: "You are not authenticated."}), false);
                else cb(null, user);
            })
        ));
        passport.use('admin-token', new JwtStrategy(jwt.opts, (jwtPayload, cb) =>
            User.findById(jwtPayload._id, (error, user) => {
                if (error) cb(error, false);
                else if (!user) cb(createError(403, {message: "You are not authenticated."}), false);
                else if (!user.admin) cb(createError(403, {message: "You are not authorized"}), false);
                else cb(null, user);
            })
        ));
        passport.use('barber-token', new JwtStrategy(jwt.opts, (jwtPayload, cb) =>
            Barber.findById(jwtPayload._id, (error, barber) => {
                if (error) cb(error, false);
                else if (!barber) cb(createError(403, {message: "You are not authenticated."}), false);
                else cb(null, barber);
            })
        ));
    },
    registerUser: (isAdmin) => async (req, res, next) => User.register(new User({
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            admin: isAdmin
        }),
        req.body.password,
        registerCb(res, next)),

    registerBarber: async (req, res, next) => Barber.register(new Barber({
            username: req.body.username,
            firstName: req.body.firstName,
            lastName: req.body.lastName,
        }),
        req.body.password,
        registerCb(res, next)),
    //verify login info
    login: (strategy) => (req, res, next) =>
        passport.authenticate(strategy, (err, user, info) => {
            if (err) return next(err);
            if (!user)
                return next(createError(500, {
                    message: 'Login failed!. ' + (info.message || "Invalid username or password.")
                }));
            res.status(200).send({
                message: 'Login successful!.',
                user: user.toJSON(),//user or barber
                token: jwt.generateToken({_id: user._id}) || "",
            });
        })(req, res, next),
    //check token
    verifyToken: (strategy) => (req, res, next) =>
        passport.authenticate(strategy, {session: false}, (error, user, info) => {
            if (error) return next(error);
            else if (!user) next(createError(500, {message: "Login failed! " + info.toString()}));
            else req.logIn(user, err =>
                    err ? next(createError(500, {message: "Login failed!. Unknown error occurred."})) : next()
                );
        })(req, res, next),
};