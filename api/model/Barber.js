/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/25/2020
 * Time: 10:25 AM
 */
const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const Schema = mongoose.Schema;

const BarberSchema = new Schema({
    //TODO add pic
    // add address & location
    username: {
        type: String,
        //TODO add errors message.
        lowercase: true,
        unique: true,
        required: [true, "can't be blank"],
        match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
        index: true
    },
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    image: String,
    days: {
        type: [{
            type: String,
            enum: ['Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri'],
        }],
        default: ['Sat', 'Sun', 'Tue', 'Wed', 'Thu', 'Fri']
    },
    //TODO add list of times
    openTime: {
        type: {
            hour: Number,
            minute: Number
        },
        default: {
            hour: 16,
            minute: 0
        }
    },
    closeTime: {
        type: {
            hour: Number,
            minute: Number
        },
        default: {
            hour: 23,
            minute: 0
        }
    },
}, {timestamps: true});

BarberSchema.plugin(passportLocalMongoose);

BarberSchema.methods.toJSON = function () {
    return {
        username: this.username,
        firstName: this.firstName||"",
        lastName: this.lastName||"",
        image: this.image || "",
        days: this.days,
        openTime: this.openTime,
        closeTime: this.closeTime
    };
};

module.exports = mongoose.model('Barber', BarberSchema);