/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/25/2020
 * Time: 10:59 AM
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AppointmentSchema = new Schema({
    barber: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Barber',
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    date: {
        type: Date,
        required: true
    },
}, {timestamps: true});

module.exports = mongoose.model('Appointment', AppointmentSchema);