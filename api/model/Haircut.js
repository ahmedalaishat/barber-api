/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/25/2020
 * Time: 10:32 AM
 */
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const HaircutSchema = new Schema({
    barber: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Barber',
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    type: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
}, {timestamps: true});

module.exports = mongoose.model('Haircut', HaircutSchema);