/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 9/23/2020
 * Time: 4:05 PM
 */
const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
    username: {
        type: String,
        //TODO add errors message.
        lowercase: true,
        unique: true,
        required: [true, "can't be blank"],
        match: [/^[a-zA-Z0-9]+$/, 'is invalid'],
        index: true
    },
    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    admin: {
        type: Boolean,
        default: false
    },
    image: String,
}, {timestamps: true});

UserSchema.plugin(passportLocalMongoose);

UserSchema.methods.toJSON = function () {
    return {
        username: this.username,
        firstName: this.firstName||"",
        lastName: this.lastName||"",
        admin: this.admin,
        image: this.image || ""
    };
};

module.exports = mongoose.model('User', UserSchema);