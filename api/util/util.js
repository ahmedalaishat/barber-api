/**
 * Created by WebStorm
 * Author: Ahmed Marwan
 * Date: 4/17/2020
 * Time: 1:39 AM
 */
const mongoose = require('mongoose');

module.exports = {
    initDB: () => {
        const url = process.env.MONGO_URL;
        const connect = mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true, useCreateIndex: true,
        });
        connect.then((db) => {
            console.log(db.connections[0].name + ' database connected correctly to the server');
        }, (error) => {
            console.log(error);
        });
    },
    errorHandler: (err, req, res, next) => {
        console.log(err.message);
        res.status(err.status || 500).send({
            status: err.status || 500,
            message: err.message
        });
    }
};