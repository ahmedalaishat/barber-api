const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');

const indexRouter = require('./api/routes/index');
const adminRouter = require('./api/routes/admin');
const barberRouter = require('./api/routes/barber');
const userRouter = require('./api/routes/user');
const haircutRouter = require('./api/routes/haircut');
const appointmentRouter = require('./api/routes/appointment');
const util = require('./api/util/util');
const authController = require('./api/controller/AuthController');
const dotenv = require('dotenv');
dotenv.config();

const app = express();

app.use(logger('dev'));
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());

util.initDB();
authController.init();

app.use('/', indexRouter);
app.use('/admin', adminRouter);
app.use('/barber', barberRouter);
app.use('/user', userRouter);
app.use('/haircut', haircutRouter);
app.use('/appointment', appointmentRouter);

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};
//
//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });


app.use((req, res, next) => next(createError(404)));
app.use(util.errorHandler);

module.exports = app;


// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};
//
//   // render the error page
//   res.status(err.status || 500);
//   res.render('error');
// });
