# [Barber API](https://barber-appointment-app.herokuapp.com/)
[Barber API Link](https://barber-appointment-app.herokuapp.com/)

## Register as a User:
Register and get a token.
- Endpoints ``/user/register``
- Method ``POST``
- The request body should be something like this:
```json
{
    "username":"ahmed",
    "firstName": "Ahmed",
    "lastName": "Marwan",
    "password":"12345678"
}
```
- The response body should be like this:
```json
{
    "message": "Registration Successful!.",
    "barber": {
        "username": "ahmed",
        "firstName": "Ahmed",
        "lastName": "Marwan",
        "admin": false,
        "image": ""
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjcwYTFhMDNiYmMzZDBjOTgxYWIzYjYiLCJpYXQiOjE2MDEyMTY5MjgsImV4cCI6MjQ2NTIxNjkyOH0.zZ__zMz1-0yxZ8IVgRbhc2bimcFROwDl0dHNI_CLAR0"
}
```

## Login as a User:
Get a token.
- Endpoints ``/user/login``
- Method ``POST``
- Request body:
```json
{
    "username":"ahmed",
    "password":"12345678"
}
```
- Response body:
```json
{
    "message": "Login successful!.",
    "user": {
        "username": "ahmed",
        "firstName": "Ahmed",
        "lastName": "Marwan",
        "admin": false,
        "image": ""
    },
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjcwYTFhMDNiYmMzZDBjOTgxYWIzYjYiLCJpYXQiOjE2MDEyMTcwMTQsImV4cCI6MjQ2NTIxNzAxNH0.Y-jxb3DAGq5txysxh6nW14xmWnCIonWxxJ3W3p-BWhg"
}
```

## Register as a Barber:
- Endpoints ``/barber/register``
- Method ``POST``
- Request body:
```json
{
    "username":"hadi",
    "firstName": "hadi",
    "lastName": "mohammed",
    "password":"87654321"
}
```

## Login as a Barber:
Get a token.
- Endpoints ``/barber/login``
- Method ``POST``
- Request body:
```json
{
    "username":"hadi",
    "password":"87654321"
}
```

## Using Token:
- put your token into Authorization field in the request Headers as the following:

```
Authorization: "bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZjcwYTFhMDNiYmMzZDBjOTgxYWIzYjYiLCJpYXQiOjE2MDEyMTcwMTQsImV4cCI6MjQ2NTIxNzAxNH0.Y-jxb3DAGq5txysxh6nW14xmWnCIonWxxJ3W3p-BWhg"
```

## Show Barber Profile:
Get a token.
- Endpoints ``/barber/show-profile``
- Method ``GET``

## Show User Profile:
Get a token.
- Endpoints ``/user/show-profile``
- Method ``GET``

## Edit Barber Profile:
Get a token.
- Endpoints ``/barber/edit-profile``
- Method ``PUT``

## Edit User Profile:
Get a token.
- Endpoints ``/user/edit-profile``
- Method ``PUT``

## Appointment Booking:
- Endpoints ``/appointment``
- Method ``POST``
- Privilege ``user``
- Request body:
```json
{
    "barber":"5f6f5a63b9c3992f9059a93b",
    "date":"2016-06-26T18:57:35.012Z"
}
```

## Show a Specific Appointment:
- Endpoints ``/appointment/:ID``
- Method ``GET``

## Show Barber Appointments:
- Endpoints ``/appointment/barber``
- Method ``GET``
- Privilege ``barber``

## Show User Appointments:
- Endpoints ``/appointment/user``
- Method ``GET``
- Privilege ``user``

## Add New Haircut:
- Endpoints ``/haircut``
- Method ``POST``
- Privilege ``barber``
- Request body:
```json
{
    "user": "5f6f5fff52d1d41e18ccd1be",
    "type":"Rounded Bob with Long Bangs",
    "description":"Rounded Bob with Long Bangs"
}
```

## show a Specific Appointment:
- Endpoints ``/haircut/:ID``
- Method ``GET``

## Show All Barber Haircuts:
- Endpoints ``/haircut/barber``
- Method ``GET``
- Privilege ``barber``

## Show All User Haircuts:
- Endpoints ``/haircut/user``
- Method ``GET``
- Privilege ``user``